<?php
function showMessage()
{
    if (($_POST['email']) && ($_POST['password']) && ($_POST['firstName']) && ($_POST['secondName']) &&
        $_POST['address'] && ($_POST['city']) && ($_POST['zip']) && ($_POST['state'])) {
        echo '<h4>Проверьте ваши данные: </h4>' .
            'Почта: ' . $_POST['email'] . '<br>' .
            'Пароль: ' . $_POST['password'] . '<br>' .
            'Имя: ' . $_POST['firstName'] . '<br>' .
            'Фамилия: ' . $_POST['secondName'] . '<br>' .
            'Адрес: ' . $_POST['address'] . '<br>' .
            'Город: ' . $_POST['city'] . '<br>' .
            'Индекс: ' . $_POST['zip'] . '<br>' .
            'Страна: ' . $_POST['state'] . '<br>';
    }
    if (($_POST['delivery']) == 'on') {
        echo 'Адрес доставки совпадает с платежным адресом' . '<br>';
    }
    if (($_POST['payment']) == 'card') {
        echo 'Оплата картой' . '<br>';
    } elseif (($_POST['payment']) == 'cash') {
        echo 'Оплата наложенным платежом' . '<br>';
    }
    include 'form.html';
}

showMessage();