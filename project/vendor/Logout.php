<?php

session_start();

class Logout
{
    public static function doLogout()
    {
        session_destroy();
        header('Location: ../index.php');
    }
}

Logout::doLogout();
