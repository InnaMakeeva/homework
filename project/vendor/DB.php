<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

class DB
{
    private static $dbObject;
    private $dbConnection;

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __construct()
    {
        try {
            $this->dbConnection = new \PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME,
                DB_USER, DB_PASSWORD);
        } catch (PDOException $e) {
            print "Ошибка подключения к базе данных!" . $e->getMessage();
            die();
        }
    }

    public static function getDbObject()
    {
        if (!isset(self::$dbObject)) {
            self::$dbObject = new DB();
        }
        return self::$dbObject->dbConnection;
    }

}

$db = DB::getDbObject();

