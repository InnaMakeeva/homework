<?php

session_start();

require_once 'DB.php';
require_once 'AbstractModel.php';

class Signup extends AbstractModel
{
    public function doSignup()
    {
        $fullName = $_POST['full_name'];
        $login = $_POST['login'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $passwordConfirm = $_POST['password_confirm'];

        if ($password === $passwordConfirm) {
            $password = md5($password);

            if ($this->loginAlreadyExists($login)) {
                $_SESSION['message'] = 'Пользователь с таким логином уже существует!';
                header('Location: ../register.php');
            } elseif ($this->emailAlreadyExists($email)) {
                $_SESSION['message'] = 'Пользователь с таким email уже существует!';
                header('Location: ../register.php');
            } else {
                $this->registerUser($fullName, $login, $email, $password);
                $_SESSION['message'] = 'Регистрация прошла успешно!';
                header('Location: ../index.php');
            }
        } else {
            $_SESSION['message'] = 'Пароли не совпадают';
            header('Location: ../register.php');
        }
    }

    private function registerUser($fullName, $login, $email, $password)
    {
        $sql = "INSERT INTO `users` (`full_name`, `login`, `email`, `password`) 
                     VALUES (:full_name, :login, :email, :password)";
        $params = ['full_name' => $fullName,'login' => $login, 'email' => $email, 'password' => $password];
        $this->executeSql($sql, $params);
    }

    private function loginAlreadyExists($login)
    {
        $sql = "SELECT * FROM `users` WHERE `login` = :login";
        $params = ['login' => $login];
        return $this->executeSql($sql, $params)->fetch();
    }

    private function emailAlreadyExists($email)
    {
        $sql = "SELECT * FROM `users` WHERE `email` = :email";
        $params = ['email' => $email];
        return $this->executeSql($sql, $params)->fetch();
    }
}

$signup = new Signup($db);
$signup->doSignup();
