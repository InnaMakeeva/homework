<?php

abstract class AbstractModel
{
    /**
     * @var PDO
     */
    public $dbConnection;

    public function __construct($dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    public function executeSql($sql, $params = [])
    {
        try {
            $query = $this->dbConnection->prepare($sql);
            $query->execute($params);
            return $query;
        } catch (\Exception $exception) {
            $_SESSION['message'] = 'Что-то пошло не так!';
            header('Location: ../index.php');
        }
    }
}
