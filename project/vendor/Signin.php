<?php

session_start();

require_once 'DB.php';
require_once 'AbstractModel.php';

class Signin extends AbstractModel
{
    public function doSignin()
    {
        $login = $_POST['login'];
        $password = md5($_POST['password']);

        $user = $this->getUser($login, $password);

        if ($user == false) {
            $_SESSION['message'] = 'Неверный логин или пароль';
            header('Location: ../index.php');
        } else {
            $_SESSION['user'] = [
                'full_name' => $user['full_name'],
                'login' => $user['login']
            ];
            header('Location: ../profile.php');
        }

    }

    private function getUser($login, $password)
    {

        $sql = "SELECT * 
                  FROM `users` 
                 WHERE `login` = :login 
                   AND `password` = :password";
        $params = ['login' => $login, 'password' => $password];
        return $this->executeSql($sql, $params)->fetch();
    }
}

$signin = new Signin($db);
$signin->doSignin();



