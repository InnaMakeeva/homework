<?php

session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/DB.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/AbstractModel.php';

if (!$_SESSION['user']) {
    header('Location: ../index.php');
}

class TaskModel extends AbstractModel
{
    public function saveTask()
    {
        $task = $_POST['task'];

        switch ($_POST['source']) {
            case 'db':
                $this->saveTaskToDBSource($task);
                break;
            case 'txt':
                $this->saveTaskToTxtSource($task);
                break;
            case 'csv':
                $this->saveTaskToCsvSource($task);
                break;
            default :
                $_SESSION['message'] = 'Вы не указали тип источника!';
                header('Location: ../task.php');
                break;
        }
    }

    private function saveTaskToTxtSource($task)
    {
        if (empty($_SESSION['sources']) || empty($_SESSION['sources']['txt'])) {
            $_SESSION['message'] = 'Вы не указали файл TXT!';
            header('Location: ../task.php');
        } else{
            $txtFileName = $_SESSION['sources']['txt'];
            if (!file_exists($txtFileName)) {
                file_put_contents($txtFileName, '');
            }
            $txtFileContent = file_get_contents($txtFileName);
            $txtFileContent .= $task . "\n";
            file_put_contents($txtFileName, $txtFileContent);
            header('Location: ../tasks-list.php');
        }
    }

    private function saveTaskToCsvSource($task)
    {
        if (empty($_SESSION['sources']) || empty($_SESSION['sources']['csv'])) {
            $_SESSION['message'] = 'Вы не указали файл CSV!';
            header('Location: ../task.php');
        } else{
            $csvFileName = $_SESSION['sources']['csv'];
            if (!file_exists($csvFileName)) {
                file_put_contents($csvFileName, '');
            }
            $csvFileContent = file_get_contents($csvFileName);
            $csvFileContent .= $task . "\n";
            file_put_contents($csvFileName, $csvFileContent);
            header('Location: ../tasks-list.php');
        }
    }

    private function saveTaskToDBSource($task)
    {
        if (empty($_SESSION['sources']) || empty($_SESSION['sources']['table'])) {
            $_SESSION['message'] = 'Вы не указали таблицу в БД!';
            header('Location: ../task.php');
        } else {
            $tableName = $_SESSION['sources']['table'];
            $userLogin = $_SESSION['user']['login'];

            $sqlCreateTable = "CREATE TABLE IF NOT EXISTS `$tableName` (
                        `id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                        `task` VARCHAR(50) NOT NULL,
                        `login` VARCHAR(50) NOT NULL)";
            $this->executeSql($sqlCreateTable);

            $sqlInsertToTable = "INSERT INTO `$tableName` (`task`, `login`) 
                                      VALUES (:task, :userLogin)";
            $params = ['task' => $task,'userLogin' => $userLogin];
            $this->executeSql($sqlInsertToTable, $params);
            header('Location: ../tasks-list.php');
        }
    }
}

$taskHandler = new TaskModel($db);
$taskHandler->saveTask();
