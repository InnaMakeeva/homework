<?php

session_start();

class SourceModel
{
    public static function storeSourceData()
    {
        $csvFileName = '';
        if ($_POST['csv']) {
            $csvFileName = $_POST['csv'];
            if (!preg_match('/(\.csv)$/i', $csvFileName)) {
                $csvFileName .= '.csv';
            }
        }
        $txtFileName = '';
        if ($_POST['txt']) {
            $txtFileName = $_POST['txt'];
            if (!preg_match('/(\.txt)$/i', $txtFileName)) {
                $txtFileName .= '.txt';
            }
        }

        $_SESSION['sources'] = [
            'txt' => $txtFileName,
            'csv' => $csvFileName,
            'table' => $_POST['table']
        ];
        header('Location: ../task.php');
    }
}

SourceModel::storeSourceData();

