<?php

session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/DB.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/AbstractModel.php';

class TaskListModel extends AbstractModel
{
    public function getTasksList()
    {
        $tasksFullList = '';
        $basePath = $_SERVER["DOCUMENT_ROOT"] . '/models/';

        if (!empty($_SESSION['sources']['txt'])) {
            $txtFileName = $_SESSION['sources']['txt'];
            if ($txtFileName && file_exists($basePath . $txtFileName)) {
                $lines = file($basePath . $txtFileName);
                foreach ($lines as $line) {
                        $tasksFullList .= '<li>' . 'TXT: ' . $line . '</li>';
                }
            }
        }
        if (!empty($_SESSION['sources']['csv'])) {
            $csvFileName = $_SESSION['sources']['csv'];
            if ($csvFileName && file_exists($basePath . $csvFileName)) {
                $lines = file($basePath . $csvFileName);
                foreach ($lines as $line) {
                        $tasksFullList .= '<li>' . 'CSV: ' . $line . '</li>';
                }
            }
        }
        if (!empty($_SESSION['sources']['table'])) {
            $table = $_SESSION['sources']['table'];
            $login = $_SESSION['user']['login'];

            $dbTasksSql = "SELECT `task` FROM `$table` WHERE `login` = '$login'";
            $tasksList = $this->executeSql($dbTasksSql)->fetchAll();
            if ($tasksList && count($tasksList)) {
                foreach ($tasksList as $row) {
                    if (isset($row['task'])) {
                        $tasksFullList .= '<li>' . 'DB: ' . $row['task'] . '</li>';
                    }
                }
            }
        }

        if ($tasksFullList) {
            $tasksFullList = '<ol>' . $tasksFullList. '</ol>';
        } else {
            $tasksFullList = '<p>К сожалению, у вас пока нет сохраненных дел. Пожалуйста, укажите <a href="profile.php" class="logout">другой источник</a> либо <a href="task.php" class="logout">создайте новые задачи</a>.</p>';
        }

        return $tasksFullList;
    }
}

$tasksListModel = new TaskListModel($db);
$tasksList = $tasksListModel->getTasksList();

