<?php

require_once 'models/TaskListModel.php';

if (!$_SESSION['user']) {
    header('Location: index.php');
}

?>
<!doctype html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Список дел</title>
      <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="tasks-list">
        <h3 style="margin-bottom: 20px;text-align: center;">
          Ваш список дел, <br> <?= $_SESSION['user']['full_name'] ?>:
        </h3>
        <p>
            <?php
             echo $tasksList;
            ?>
        </p>
        <p class="small-text border">
            <a href="task.php" class="logout">Добавить задание</a>
        </p>
        <p class="small-text">
            <a href="profile.php" class="logout">Изменить источник</a>
        </p>
        <p class="small-text">
            <a href="vendor/Logout.php" class="logout">Выход из аккаунта</a>
        </p>
    </div>
  </body>
</html>

