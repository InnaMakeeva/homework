<?php

session_start();

if (!$_SESSION['user']) {
    header('Location: index.php');
}
$txtFileName = '';
$csvFileName = '';
$tableName = '';
if (!empty($_SESSION['sources'])) {
    $txtFileName = $_SESSION['sources']['txt'];
    $csvFileName = $_SESSION['sources']['csv'];
    $tableName = $_SESSION['sources']['table'];
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Кабинет</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<form action="models/SourceModel.php" method="post">
    <h3 style="margin-bottom: 20px;text-align: center;">
      Ваши активные источники данных, <br> <?= $_SESSION['user']['full_name'] ?>
    </h3>
    <label>Название TXT-файла</label>
    <input value="<?= $txtFileName ?>" type="text" name="txt" maxlength="45" placeholder="name">
    <label>Hазвание CSV-файла</label>
    <input value="<?= $csvFileName ?>" type="text" name="csv" maxlength="45" placeholder="name">
    <label>Hазвание таблицы для БД</label>
    <input value="<?= $tableName ?>" type="text" name="table" maxlength="45" placeholder="name">
    <div>
        <button type="reset" style="width:49%">Очистить</button>
        <button class="save" type="submit" style="width:49%">Сохранить</button>
    </div>
    <p class="small-text border">
      <a href="task.php" class="logout">Добавить задание</a>
    </p>
    <p class="small-text">
      <a href="tasks-list.php" class="logout">Список дел</a>
    </p>
    <p class="small-text">
      <a href="vendor/Logout.php" class="logout">Выход из аккаунта</a>
    </p>
</form>
</body>
</html>
