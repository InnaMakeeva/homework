<?php

session_start();

if (!empty($_SESSION['user'])) {
    header('Location: profile.php');
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Авторизация</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<form action="vendor/Signin.php" method="post">
    <label>Логин</label>
    <input type="text" name="login" maxlength="45" placeholder="Введите логин" required>
    <label>Пароль</label>
    <input type="password" name="password" maxlength="20" placeholder="Введите пароль" required>
    <button type="submit">Войти</button>
    <p>
        У вас нет аккаунта? - <a href="/register.php">Зарегистрируйтесь!</a>
    </p>
    <?php
    if (!empty($_SESSION['message'])) {
        echo '<p class="msg"> ' . $_SESSION['message'] . ' </p>';
    }
    unset($_SESSION['message']);
    ?>
</form>
</body>
</html>
