<?php

session_start();

if (!empty($_SESSION['user'])) {
    header('Location: profile.php');
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Регистрация</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<form action="vendor/Signup.php" method="post">
    <label>ФИО</label>
    <input type="text" name="full_name" maxlength="45" placeholder="Введите ФИО" required>
    <label>Логин</label>
    <input type="text" name="login" maxlength="45" placeholder="Введите логин" required>
    <label>Почта</label>
    <input type="email" name="email" maxlength="45" placeholder="Введите адрес почты" required>
    <label>Пароль</label>
    <input type="password" name="password" minlength="6" maxlength="20" placeholder="Введите пароль" required>
    <label>Подтверждение пароля</label>
    <input type="password" name="password_confirm" minlength="6" maxlength="20" placeholder="Подтвердите пароль" required>
    <div>
        <button type="reset" style="width:49%">Очистить</button>
      <button type="submit" class="save" style="width:49%">Зарегистрироваться</button>
    </div>
    <p>
        Уже есть аккаунт? - <a href="/">Aвторизируйтесь!</a>
    </p>
    <?php
    if (!empty($_SESSION['message'])) {
        echo '<p class="msg"> ' . $_SESSION['message'] . ' </p>';
    }
    unset($_SESSION['message']);
    ?>
</form>
</body>
</html>
