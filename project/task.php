<?php

session_start();

if (!$_SESSION['user']) {
    header('Location: index.php');
}
?>
<!doctype html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Задание</title>
      <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <form action="models/TaskModel.php" method="post">
      <h3 style="margin-bottom: 20px;text-align: center;">
        Добрый день, <br> <?= $_SESSION['user']['full_name'] ?>
      </h3>
      <label>Список дел</label>
      <textarea style="margin: 15px 0;" rows="5" name="task" maxlength="45" placeholder="Задание" required></textarea>
      <label>Выберите источник</label>
      <select name="source">
          <option value="txt">TXT</option>
          <option value="csv">CSV</option>
          <option value="db">DB</option>
      </select>
      <div>
          <button type="reset" style="width:49%">Очистить</button>
          <button class="save" type="submit" style="width:49%">Сохранить</button>
      </div>
      <p class="small-text border">
          <a href="profile.php" class="logout">Изменить источник</a>
      </p>
      <p class="small-text">
        <a href="tasks-list.php" class="logout">Список дел</a>
      </p>
      <p class="small-text">
          <a href="vendor/Logout.php" class="logout">Выход из аккаунта</a>
      </p>
      <?php
      if (!empty($_SESSION['message'])) {
          echo '<p class="msg"> ' . $_SESSION['message'] . ' </p>';
      }
      unset($_SESSION['message']);
      ?>
    </form>
  </body>
</html>

