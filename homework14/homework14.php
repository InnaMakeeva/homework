<?php
interface canDrive
{
    function drive();
}

abstract class Cars implements canDrive
{
    public $color,$mark,$model;

    public function get_info(){
        $properties = ['color','mark','model'];
        $res=[];
        foreach ($properties as $prop){
            $res[] = $this->$prop;
        }
        return implode(' ', $res);
    }
    function drive(){
        return 'I am driving ' . $this->get_info();
    }
}
class Ford extends Cars
{
    function drive()
    {
        echo parent::drive() . '. It is sadly.' . ' ';
    }
}
class Bmw extends Cars
{
    function drive()
    {
        echo parent::drive() . '. It is cool.' . ' ';
    }
}
class Person {

    public $name, $surname;
    private $car;

    function sit($car) {
        if(!is_object($car)) return;
        $this->car = $car;
    }

    function drive(){
        if(is_object($this->car)) {
            $this->car->drive();
        } else echo 'I do not have a car';
    }
    function get_info(){
        echo 'I am ' . $this->name . ' ' . $this->surname . '. ';
        echo $this->drive() . '</br>';
    }
}

$ford = new Ford();                
$ford->color = 'White';
$ford->mark = 'Ford';
$ford->model = 'Transit';

$bmw = new Bmw();
$bmw->color = 'Black';
$bmw->mark = 'BMW';
$bmw->model = 'X5';

$Sasha = new Person();
$Sasha->name = 'Sasha';
$Sasha->surname = 'Ivanov';
$Sasha->sit($ford);
$Sasha->get_info();

$Dima = new Person();
$Dima->name = 'Dima';
$Dima->surname = 'Petrov';
$Dima->sit($bmw);
$Dima->get_info();