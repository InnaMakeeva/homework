<?php

class Faculty
{
    protected $title;

    public function set_title($title)
    {
        $this->title = $title;
        return $this;
    }
    public function get_title()
    {
        return $this->title;
    }
    public function get_info()
    {
        return 'Faculty: ' . $this->title . '</br>';
    }
}

class Speciality extends Faculty
{
    protected $titleSpeciality;

    public function set_titleSpeciality($titleSpeciality)
    {
        $this->titleSpeciality = $titleSpeciality;
        return $this;
    }
    public function get_titleSpeciality()
    {
        return $this->titleSpeciality;
    }
    public function get_info()
    {
        return parent::get_info() . 'Speciality: ' . $this->titleSpeciality . '</br>';
    }
}

class Group extends Speciality
{
    protected $titleGroup;

    public function set_titleGroup($titleGroup)
    {
        $this->titleGroup = $titleGroup;
        return $this;
    }
    public function get_titleGroup()
    {
        return $this->titleGroup ? $this->titleGroup . '</br>' : ' unknown group' . '</br>';
    }
    public function get_info()
    {
        return parent::get_info() . 'Group: ' . $this->get_titleGroup() . '</br>';
    }
}

class Student extends Group
{
    public $name, $surname, $age, $gender;

    public function get_info()
    {
        return 'Student: ' . '</br>' . ' Name: ' . $this->name . '. Surname: ' . $this->surname .
            '. Age: ' . $this->age . '. Gender: ' . $this->gender . '</br>' . parent::get_info();
    }
}

$std1 = new Student();
$std1->name = 'Ivan';
$std1->surname = 'Ivanov';
$std1->age = 18;
$std1->gender = 'Male';
$std1->set_titleSpeciality('Finance');
$std1->set_title('Economic');

echo $std1->get_info();
