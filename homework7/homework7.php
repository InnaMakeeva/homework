<?php
//1. Найти минимальное и максимальное среди 3 чисел

//function expression

$findMinMax = function ($x = 3, $y = 1, $z = 8) {
    $arr = [$x, $y, $z];
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++)
        $count = $i;
    foreach ($arr as $value) {
        for ($j = 0; $j < $count; $j++) {
            if ($arr[$j] > $arr[$j + 1]) {
                $num = $arr[$j + 1];
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $num;
            }
        }
    }
    if ($arr[0] == $arr[$count]) {
        echo $arr[0] . ' = ' . $arr[$count] . '<br>';
    } else {
        echo "$arr[0] - наименьшее число, $arr[$count] - наибольшее число <br>";
    }
};
$findMinMax(5,5,5);
$findMinMax();

//Стрелочная функция

function findMinMax($x, $y, $z){
    $arr = [$x, $y, $z];
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++)
        $count = $i;
    foreach ($arr as $value) {
        for ($j = 0; $j < $count; $j++) {
            if ($arr[$j] > $arr[$j + 1]) {
                $num = $arr[$j + 1];
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $num;
            }
        }
    }
    if ($arr[0] == $arr[$count])
        echo $arr[0] . ' = ' . $arr[$count] . '<br>';
    else
        echo "$arr[0] - наименьшее число, $arr[$count] - наибольшее число <br>";
}
$findMinMaxArrow = fn ($x = 1, $y = 1, $z =1) => findMinMax($x, $y, $z);
$findMinMaxArrow(5,7,1);
$findMinMaxArrow();

// или

$findMaxArrow = fn (...$values) => max(...$values);
echo $findMaxArrow(20,4,6,8);

$findMinArrow = fn (...$values) => min(...$values);
echo $findMinArrow(20,4,6,8);


//2. Найти площадь

//function expression

$calcArea = function ($a = 10, $b = 4)
{
    return "Площадь фигуры со сторонами $a и $b  составляет " .
        $a * $b . '<br>';
};
echo $calcArea(5, 5);

//Стрелочная функция

$calcArea2 = fn ($a = 10, $b = 4) =>  $a * $b;
echo "Площадь = " . $calcArea2(5, 5);

//3. Теорема Пифагора

//function expression

$calkHypotenuse = function ($leg1 = 3, $leg2 = 4){
    $hypotenuse = sqrt($leg1**2 + $leg2**2);
    return "Гипотенуза треугольника с катетами $leg1 и $leg2 равна $hypotenuse";
};
echo $calkHypotenuse();

//Стрелочная функция

$calkHypotenuse = fn($leg1 = 3, $leg2 = 4) => sqrt($leg1**2 + $leg2**2);
echo "Гипотенуза = " . $calkHypotenuse();

//4. Найти периметр

//function expression

$calkPerimeter = function ($a = 10, $b = 4)
{
    return "Периметр фигуры со сторонами $a и $b  составляет " .
        ($a + $b) * 2 . '<br>';
};
echo $calkPerimeter(5,3);

//Стрелочная функция

$calkPerimeter = fn ($a = 10, $b = 4) =>  ($a + $b) * 2 ;
echo "Периметр = " . $calkPerimeter();

//5. Найти дискриминант

//function expression

$calkDiscriminant = function ($a = 1, $b = 6, $c = 9)
{
    $discriminant = $b**2 - 4*$a*$c;
    return "В уравнении axx + bx + c = 0 дискриминант равен $discriminant <br>";
};
echo $calkDiscriminant(3,4,2);
echo $calkDiscriminant(1,-4,-5);

//Стрелочная функция

$calkDiscriminant = fn ($a = 1, $b = 6, $c = 9) => $b**2 - 4*$a*$c;
echo "В уравнении axx + bx + c = 0 дискриминант равен " .  $calkDiscriminant(3,4,2);

//6. Создать только четные числа до 100

//function expression

$createEven = function ()
{
    for ($i = 2; $i <= 100; $i += 2) {
        echo $i . '<br>';
    }
};
$createEven();

//Стрелочная функция

function createEven()
{
    for ($i = 2; $i <= 100; $i += 2) {
        echo $i . '<br>';
    }
}
$createEvenArrow = fn () => createEven();
$createEvenArrow();

// 7. Создать нечетные числа до 100

//function expression

$createOdd = function ()
{
    for ($i = 1; $i <= 100; $i += 2) {
        echo $i . '<br>';
    }
};
$createOdd();

//Стрелочная функция

function createOdd()
{
    for ($i = 1; $i <= 100; $i += 2) {
        echo $i . '<br>';
    }
}
$createOddArrow = fn () => createOdd();
$createOddArrow();


// 8. Определите, есть ли в массиве повторяющиеся элементы.

//function expression

$findRepetitions1 = function ($x = 9, $y = 0, $z = 11, $a = 3, $b = 'q', $c = 'v')
{
    $arr = [$x, $y, $z, $a, $b, $c];
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++) {
        $count = $i;
    }
    for ($i = 0; $i < $count; $i++) {
        for ($j = $i + 1; $j <= $count; $j++) {
            if ($arr[$i] === $arr[$j]) {
                $arrNew[] = $arr[$i];
                break;
            }
        }
    }
    if (empty($arrNew))
        echo 'Повторяющихся элементов в массиве нет' . '<br>';
    else
        echo 'Повторяющиеся элементы в массиве есть' . '<br>';
};

$findRepetitions1(5, 5, 5);
$findRepetitions1('v');
$findRepetitions1();

//Стрелочная функция

function findRepetitions($x, $y, $z, $a, $b, $c)
{
    $arr = [$x, $y, $z, $a, $b, $c];
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++) {
        $count = $i;
    }
    for ($i = 0; $i < $count; $i++) {
        for ($j = $i + 1; $j <= $count; $j++) {
            if ($arr[$i] === $arr[$j]) {
                $arrNew[] = $arr[$i];
                break;
            }
        }
    }
    if (empty($arrNew))
        echo 'Повторяющихся элементов в массиве нет' . '<br>';
    else
        echo 'Повторяющиеся элементы в массиве есть' . '<br>';
}

$findRepetitionsArrow = fn ($x = 9, $y = 0, $z = 11, $a = 3, $b = 'q', $c = 'v') => findRepetitions($x, $y, $z, $a, $b, $c);
$findRepetitionsArrow('v');
$findRepetitionsArrow();

//9. Сортировка

//function expression

$sortArray = function ($arr, $sort = 'adc')
{
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++) {
        $count = $i;
    }
    if ($sort == 'adc') {
        foreach ($arr as $value) {
            for ($j = 0; $j < $count; $j++) {
                if ($arr[$j] > $arr[$j + 1]) {
                    $num = $arr[$j + 1];
                    $arr[$j + 1] = $arr[$j];
                    $arr[$j] = $num;
                }
            }
        }
    } else {
        foreach ($arr as $value) {
            for ($j = 0; $j < $count; $j++) {
                if ($arr[$j] < $arr[$j + 1]) {
                    $num = $arr[$j + 1];
                    $arr[$j + 1] = $arr[$j];
                    $arr[$j] = $num;
                }
            }
        }
    }
    return $arr;
};
echo '<pre>';
print_r($sortArray([2,11,34,3,5,113],'adc'));
echo '<pre>';

//Стрелочная функция

function sortArray($arr)
{
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++) {
        $count = $i;
    }

    foreach ($arr as $value) {
        for ($j = 0; $j < $count; $j++) {
            if ($arr[$j] > $arr[$j + 1]) {
                $num = $arr[$j + 1];
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $num;
            }
        }
    }
    return $arr;
}

function rsortArray($arr)
{
    $count = 0;
    for ($i = 0; isset($arr[$i]); $i++) {
        $count = $i;
    }

    foreach ($arr as $value) {
        for ($j = 0; $j < $count; $j++) {
            if ($arr[$j] < $arr[$j + 1]) {
                $num = $arr[$j + 1];
                $arr[$j + 1] = $arr[$j];
                $arr[$j] = $num;
            }
        }
    }
    return $arr;
}

$sortArrArrow = fn ($arr, $sort = 'adc') => $sort == 'adc' ? sortArray($arr) : rsortArray($arr);
echo '<pre>';
print_r($sortArrArrow([2,40,6,80,0,50]));
echo '<pre>';

