CREATE TABLE homework11.fruits like phenix.fruits;

INSERT INTO homework11.fruits SELECT * from phenix.fruits;

select distinct title, type from fruits;

select title from fruits group by title;

select title, count(*) from fruits group by title;

select max(id), type, title from fruits group by type, title;

select max(id), type, title from fruits where length(title) > 5 group by
type, title having max(id) > 10;

select max(id), type, title from fruits where length(title) > 5 group by type,
title having max(id) > 10 order by type asc, title desc;

delete from fruits where id >= 10;

INSERT INTO fruits (title, type) VALUES ("Avocado", "Stone"), ("Peach",
"Stone");

INSERT INTO fruits (id, title, type) VALUES (10, "Avocado", "Stone"), (11, "Peach", "Stone");

update fruits set title = "Orange", type = "Citrus" where id = 20;

delete from fruits where id between 15 and 25;

select name, age from actors order by age desc;

select name, Oscars from actors where Oscars between 2 and 4;

select title, year, box_office from films where year >2000 and box_office > 2200000000;

select name, age, number_of_films from directors where age > 70 order by number_of_films desc;

select * from films limit 2, 2;

SELECT _ FROM (SELECT _ FROM films reset ORDER BY id DESC LIMIT 3 ) as r ORDER BY id;
